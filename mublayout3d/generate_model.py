import os

import cv2

import mublayout3d as ml3


def generate_model(input_path: os.PathLike, output_path: os.PathLike, wall_height: float = 100, floor_depth: float = 10) -> None:
    """
    Generates a 3D model from the input and saves the intermediate and final results to the specified output path.

    Parameters
    ----------
    input_path : PathLike
        The input path to the floor plan image
    output_path : PathLike
        The path to which the intermediate and final outputs to be saved
    wall_height : float
        The desired height of the walls, defaults to 100
    floor_depth : float
        The desired depth of the floor, defaults to 10

    """

    # Declare the save paths for the meshes
    filename = '.'.join(os.path.basename(input_path).split('.')[:-1])
    wall_mesh_path = os.path.join(output_path, f'walls_{filename}.obj')
    door_mesh_path = os.path.join(output_path, f'doors_{filename}.obj')
    combined_mesh_path = os.path.join(output_path, f'model_{filename}.obj')

    # Read the floor plan image
    image = cv2.imread(input_path)

    # Predict the walls from the image
    prediction = ml3.WallDetector().predict(image)

    # Binarize the heightmap to {0, 1}
    _, thresh = cv2.threshold(prediction, 127, 255, cv2.THRESH_BINARY)

    # Find the contours of the heightmap
    contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_TC89_KCOS)

    # Build the wall mesh from the contours
    wall_mesh = ml3.build_wall_mesh(contours, wall_height, floor_depth)

    # Save the wall mesh as OBJ file format
    wall_mesh.save_as_obj(wall_mesh_path)

    # Get the object detection results
    results = ml3.OpeningsDetector().predict(image)

    # Get intersections between walls and door bounding box
    intersections = ml3.get_intersections(results, contours, save_path=output_path)

    # Build the door mesh from the intersections
    door_mesh = ml3.build_door_mesh(intersections, wall_height)

    # Save the door mesh as OBJ file format
    door_mesh.save_as_obj(door_mesh_path)

    # Combine the wall and door meshes
    combined_mesh = ml3.combine_mesh(wall_mesh_path, door_mesh_path)

    # Save the combined mesh as OBJ file format
    combined_mesh.save_as_obj(combined_mesh_path)
