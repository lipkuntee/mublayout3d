import os
import math

import numpy as np
import cv2
import gdown
import segmentation_models as sm

import mublayout3d


DEFAULT_MODEL_FILENAME = 'unet_efficientnetb3_192p_50k_wall_12e.h5'
DEFAULT_MODEL_DOWNLOAD_URL = 'https://drive.google.com/uc?id=1JLVl-3cdeJtEJoiTP72cY91tapZgyF3V'
DEFAULT_BACKBONE = 'efficientnetb3'

MODULE_PATH = '/'.join(mublayout3d.__file__.split('\\')[:-1])
DEFAULT_MODEL_PATH = f'{MODULE_PATH}/models/{DEFAULT_MODEL_FILENAME}'


# Set to using TensorFlow Keras framework
sm.set_framework('tf.keras')


class WallDetector:
    def __init__(self, model_path: os.PathLike = DEFAULT_MODEL_PATH, backbone: str = DEFAULT_BACKBONE) -> None:
        """
        Initializes a wall segmentation model for floor plan images.

        Parameters
        ----------
        model_path : PathLike
            The path to the saved model weights
        backbone : str
            The name of the backbone to be attached to the model

        """

        # Use default weights if model path is invalid
        if not os.path.isfile(model_path):
            print(f'Invalid model file path: {model_path}, using default model weights instead.')
            model_path = DEFAULT_MODEL_PATH

        # Download the default weights from the link if file not found
        if not os.path.isfile(model_path):
            print('Default model weights not found, attempting to download from the link.')
            gdown.download(DEFAULT_MODEL_DOWNLOAD_URL, model_path)

        # Get the preprocessing function corresponding to the backbone
        self.preprocess_input = sm.get_preprocessing(backbone)

        # Construct the model network
        self.generator = sm.Unet(backbone, classes=1, activation='sigmoid')

        # Load the pretrained weights from the path
        self.generator.load_weights(model_path)


    def predict(self, image: np.array) -> np.array:
        """
        Predicts with the image as the input using the wall segmentation model.

        Parameters
        ----------
        image : array
            The target image to be predicted
            
        Returns
        -------
        array
            A predicted mask image which consists of walls only

        """

        # Predict the walls from the rasterized image
        height, width = image.shape[:2]

        # Resized the image to the size of the input layer (rounding up)
        cols, rows = math.ceil(width / 192), math.ceil(height / 192)
        ht, wt = 192 * rows, 192 * cols
        resized = cv2.resize(image, (wt, ht))

        # Split the image into few patches
        patches = []
        for y in range(96, ht - 95, 192):
            for x in range(96, wt - 95, 192):
                patches.append(resized[y-96:y+96, x-96:x+96])

        # Preprocess and predict the image in a batch of patches
        preprocessed = np.expand_dims(self.preprocess_input(np.array(patches)), -1)
        prediction = self.generator.predict(preprocessed, verbose=0, use_multiprocessing=True)

        # Resize, merge and reconstruct the patches of prediction to a single image
        prediction = prediction.reshape(rows, cols, 192, 192)
        prediction = prediction.transpose((0, 2, 1, 3))
        prediction = prediction.reshape((ht, wt))

        # Resize the final result to the original image shape
        prediction = cv2.resize(prediction.round(), (width, height))

        # Round the values and convert to uint8 format
        prediction = prediction.round().astype(np.uint8) * 255

        return prediction
