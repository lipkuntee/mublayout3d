import os
from typing import Any
import math

from shapely.geometry import Polygon, MultiPolygon
import numpy as np
import torch
import matplotlib.pyplot as plt

from .mesh import Mesh


THRESHOLD = 0.7


def get_intersections(detector_results: Any, wall_contours: Any, threshold: float = THRESHOLD, save_path: Any = None) -> list:
    """
    Gets the intersection points between the walls and the detected bounding boxes.

    Parameters
    ----------
    detector_results : Any
        The prediction results from the YOLOv5 detection model
    wall_contours : str
        The contours of the walls
    threshold : float
        The confidence threshold for which the object detection to be taken into consideration
    save_path : Any
        The save path to a plot of the intersections

    Returns
    -------
    list
        A list of the intersection points

    """

    results = detector_results.xyxy[0]
    bounding_boxes = {}

    fig = plt.figure()

    # For all results, get the ones with confidence higher than specified threshold
    for idx in torch.where(results[:, 4] > threshold)[0]:
    
        x1, y1, x2, y2, _, cls = results[idx].cpu()
        points = np.array([[x1, y1], [x2, y1], [x2, y2], [x1, y2]])
        bbox = Polygon(points)
        cls = int(cls.item())

        if cls in bounding_boxes:
            bounding_boxes[cls].append(bbox)
        else:
            bounding_boxes[cls] = [bbox]


    door_bboxes = bounding_boxes[0]
    intersections = []

    for contour in wall_contours:
        xs = [p[0][0] for p in contour]
        ys = [p[0][1] for p in contour]
        points = np.array([[xs[i], ys[i]] for i in range(len(xs))])

        if len(xs) > 3:
            wall = Polygon(points)
            plt.plot(*wall.exterior.xy)

            if not wall.is_valid:
                continue

            for box in door_bboxes:
                if wall.intersects(box):
                    intersections.append([box, wall.intersection(box)])

    for _, intersection in intersections:
        if not isinstance(intersection, MultiPolygon) or len(intersection.geoms) != 2:
            continue
            
        point1 = intersection.geoms[0].representative_point().xy
        point2 = intersection.geoms[1].representative_point().xy
        midpoint = [(point1[0][0] + point2[0][0]) / 2, (point1[1][0] + point2[1][0]) / 2]
        plt.scatter([midpoint[0]], [midpoint[1]], marker='x')

    plt.axis('off')

    if save_path != None:
        plt.savefig(os.path.join(save_path, 'intersections.png'))

    plt.close(fig)

    return intersections


def get_door_mesh_data(intersections: list, wall_height: float, door_width: float = 1, height_scale: float = 0.75) -> tuple:
    """
    Gets the intersection points between the walls and the detected bounding boxes.

    Parameters
    ----------
    intersections : list
        The intersection points between the walls and the bounding boxes
    wall_height : float
        The height of the 3D model object
    door_width : float
        The width of the doors, defaults to 1
    height_scale : float
        The relative height of the doors with respect to the model's height, defaults to 0.75

    Returns
    -------
    tuple
        A tuple of vertices and faces of the generated door meshes

    """

    door_faces = []

    for _, intersection in intersections:

        # If the number of intersections is not equal to 2, then it is not a valid detection
        if not isinstance(intersection, MultiPolygon) or len(intersection.geoms) != 2:
            continue
        
        point1 = intersection.geoms[0].representative_point().xy
        point2 = intersection.geoms[1].representative_point().xy

        deg = math.atan2(-(point2[1][0] - point1[1][0]), point2[0][0] - point1[0][0])

        dx = door_width * math.sin(deg)
        dy = door_width * math.cos(deg)

        ########## BASE DOOR MESH START ##########

        x1 = point1[0][0]
        y1 = point1[1][0]
        x2 = x1 + dx
        y2 = y1 + dy

        x3 = point2[0][0]
        y3 = point2[1][0]
        x4 = x3 + dx
        y4 = y3 + dy

        h = height_scale * wall_height

        door_faces += [
            [(x2, y2, h), (x2, y2, 0), (x1, y1, 0)],
            [(x1, y1, 0), (x1, y1, h), (x2, y2, h)],
            [(x3, y3, h), (x1, y1, h), (x1, y1, 0)],
            [(x1, y1, 0), (x3, y3, 0), (x3, y3, h)],
            [(x4, y4, h), (x3, y3, h), (x3, y3, 0)],
            [(x3, y3, 0), (x4, y4, 0), (x4, y4, h)],
            [(x4, y4, h), (x2, y2, h), (x2, y2, 0)],
            [(x2, y2, 0), (x4, y4, 0), (x4, y4, h)],
            [(x4, y4, h), (x2, y2, h), (x1, y1, h)],
            [(x1, y1, h), (x3, y3, h), (x4, y4, h)],
            [(x4, y4, 0), (x3, y3, 0), (x1, y1, 0)],
            [(x1, y1, 0), (x2, y2, 0), (x4, y4, 0)],
        ]

        ########## BASE DOOR MESH END ##########

        ########## DOOR KNOB MESH START ##########

        doorknob_coord_1 = (x1 + 2 * (x3 - x1) / 3, y1 + 2 * (y3 - y1) / 3)
        doorknob_coord_2 = (x1 + 5 * (x3 - x1) / 6, y1 + 5 * (y3 - y1) / 6)

        x1 = doorknob_coord_1[0] + 2 * dx
        y1 = doorknob_coord_1[1] + 2 * dy
        x2 = x1 - 3 * dx
        y2 = y1 - 3 * dy

        x3 = doorknob_coord_2[0] + 2 * dx
        y3 = doorknob_coord_2[1] + 2 * dy
        x4 = x3 - 3 * dx
        y4 = y3 - 3 * dy

        h = 0.5 * height_scale * wall_height
        dh = 0.01 * height_scale * wall_height

        door_faces += [
            [(x2, y2, h), (x2, y2, h - dh), (x1, y1, h - dh)],
            [(x1, y1, h - dh), (x1, y1, h), (x2, y2, h)],
            [(x3, y3, h), (x1, y1, h), (x1, y1, h - dh)],
            [(x1, y1, h - dh), (x3, y3, h - dh), (x3, y3, h)],
            [(x4, y4, h), (x3, y3, h), (x3, y3, h - dh)],
            [(x3, y3, h - dh), (x4, y4, h - dh), (x4, y4, h)],
            [(x4, y4, h), (x2, y2, h), (x2, y2, h - dh)],
            [(x2, y2, h - dh), (x4, y4, h - dh), (x4, y4, h)],
            [(x4, y4, h), (x2, y2, h), (x1, y1, h)],
            [(x1, y1, h), (x3, y3, h), (x4, y4, h)],
            [(x4, y4, h - dh), (x3, y3, h - dh), (x1, y1, h - dh)],
            [(x1, y1, h - dh), (x2, y2, h - dh), (x4, y4, h - dh)],
        ]

        ########## DOOR KNOB MESH END ##########

    door_vertices = set()

    for f in door_faces:
        for v in f:
            door_vertices.add(v)

    door_vertices = list(door_vertices)
    door_faces = list(map(lambda x: (door_vertices.index(x[0]) + 1, door_vertices.index(x[1]) + 1, door_vertices.index(x[2]) + 1), door_faces))

    return door_vertices, door_faces

def build_door_mesh(intersections: list, wall_height: float, door_width: float = 1, height_scale: float = 0.75) -> Mesh:
    """
    Gets the intersection points between the walls and the detected bounding boxes.

    Parameters
    ----------
    intersections : list
        The intersection points between the walls and the bounding boxes
    wall_height : float
        The height of the 3D model object
    door_width : float
        The width of the doors, defaults to 1
    height_scale : float
        The relative height of the doors with respect to the model's height, defaults to 0.75

    Returns
    -------
    Mesh
        A mesh for the generated door vertices and faces

    """

    door_vertices, door_faces = get_door_mesh_data(intersections, wall_height, door_width, height_scale)
    return Mesh(door_vertices, door_faces)