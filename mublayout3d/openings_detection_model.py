from typing import Any
import os

import numpy as np
import torch
import gdown

import mublayout3d


DEFAULT_MODEL_FILENAME = 'yolov5_640p_100_wall-openings_10e.pt'
DEFAULT_MODEL_DOWNLOAD_URL = 'https://drive.google.com/uc?id=12glJ5vWmsjMq8vvH-2pRIKnmrrVPD17t'

MODULE_PATH = '/'.join(mublayout3d.__file__.split('\\')[:-1])
DEFAULT_MODEL_PATH = f'{MODULE_PATH}/models/{DEFAULT_MODEL_FILENAME}'


class OpeningsDetector:
    def __init__(self, model_path: os.PathLike = DEFAULT_MODEL_PATH) -> None:
        """
        Initializes a detector for the wall openings.

        Parameters
        ----------
        model_path : PathLike
            The path to the saved model weights

        """

        # Use default weights if model path is invalid
        if not os.path.isfile(model_path):
            print(f'Invalid openings detector model file path: {model_path}, using default model weights instead.')
            model_path = DEFAULT_MODEL_PATH

        # Download the default weights from the link if file not found
        if not os.path.isfile(model_path):
            print('Default openings detector model weights not found, attempting to download from the link.')
            gdown.download(DEFAULT_MODEL_DOWNLOAD_URL, model_path)

        self.model = torch.hub.load('ultralytics/yolov5', 'custom', path=model_path)


    def predict(self, image: np.array) -> Any:
        """
        Predicts with the image as the input using the wall openings detector.

        Parameters
        ----------
        image : array
            The target image to be predicted
            
        Returns
        -------
        Any
            An object which contains the information of the detector predictions

        """

        return self.model([image], size=640)
    