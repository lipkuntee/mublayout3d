import os

from .mesh import Mesh


def get_mesh_data(filename: os.PathLike) -> tuple:
    """
    Gets the data of vertices and faces from an OBJ file.

    Parameters
    ----------
    filename : PathLike
        The path to the OBJ file

    Returns
    -------
    tuple
        A tuple of vertices and faces of the mesh

    """

    with open(filename, 'r') as f:
        data = f.readlines()

    vertices_data = []
    faces_data = []

    for line in data:
        line = line.replace('\n', '')
        if line.startswith('v'):
            vertices_data.append(tuple(map(float, line[2:].split(' '))))
        elif line.startswith('f'):
            faces_data.append(tuple(map(int, line[2:].split(' '))))

    faces_data = list(map(lambda x: [vertices_data[x[0] - 1], vertices_data[x[1] - 1], vertices_data[x[2] - 1]], faces_data))

    return vertices_data, faces_data


def combine_mesh(mesh1_path: os.PathLike, mesh2_path: os.PathLike) -> Mesh:
    """
    Combines two meshes into a single mesh.

    Parameters
    ----------
    mesh1_path : PathLike
        The path to the first OBJ file
    mesh2_path : PathLike
        The path to the second OBJ file

    Returns
    -------
    Mesh
        A combined mesh which results from the two input meshes

    """

    vert1, face1 = get_mesh_data(mesh1_path)
    vert2, face2 = get_mesh_data(mesh2_path)

    combined_verts = set(vert1)
    combined_verts = combined_verts.union(set(vert2))
    combined_verts = list(combined_verts)
    combined_faces = face1 + face2
    combined_faces = list(map(lambda x: (combined_verts.index(x[0]) + 1, combined_verts.index(x[1]) + 1, combined_verts.index(x[2]) + 1), combined_faces))

    return Mesh(combined_verts, combined_faces)
