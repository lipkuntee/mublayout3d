import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import PolyCollection


def frustum(left, right, bottom, top, znear, zfar) -> np.array:
    """
    Creates a perspective projection matric for a frustrum-shaped viewing volume.

    Parameters
    ----------
    `left`, `right`, `bottom`, `top` : float
        The coordinates of the frustum planes
    `znear`, `zfar` : float
        The distances to the near and far clipping planes

    Returns
    -------
    array
        A 4x4 numpy array representing the projection matrix.

    """
    
    M = np.zeros((4, 4), dtype=np.float32)
    M[0, 0] = +2.0 * znear / (right - left)
    M[1, 1] = +2.0 * znear / (top - bottom)
    M[2, 2] = -(zfar + znear) / (zfar - znear)
    M[0, 2] = (right + left) / (right - left)
    M[2, 1] = (top + bottom) / (top - bottom)
    M[2, 3] = -2.0 * znear * zfar / (zfar - znear)
    M[3, 2] = -1.0

    return M


def perspective(fovy, aspect, znear, zfar):
    """
    Creates a perspective projection matrix for a perspective viewing volumne with a given
    field of view

    Parameters
    ----------
    `fovy` : float
        The vertical field of view angle in degrees
    `aspect` : float
        The aspect ratio of the viewport (width/height)
    `znear`, `zfar` : float
        The distances to the near and far clipping planes

    Returns
    -------
    array
        A 4x4 numpy array representing the projection matrix.

    """

    h = np.tan(0.5 * np.radians(fovy)) * znear
    w = h * aspect

    return frustum(-w, w, -h, h, znear, zfar)


def translate(x, y, z) -> np.array:
    """
    Creates a translation matrix that moves a point by a given amount along the x, y, and
    z axes.

    Parameters
    ----------
    `x`, `y`, `z` : float
        The amount to move the point along each axis

    Returns
    -------
    array
        A 4x4 numpy array representing the translation matrix.

    """

    return np.array([[1, 0, 0, x],
                     [0, 1, 0, y],
                     [0, 0, 1, z],
                     [0, 0, 0, 1]], dtype=float)


def xrotate(theta) -> np.array:
    """
    Creates a rotation matrix that rotates a point around the x-axis by a given angle.

    Parameters
    ----------
    `theta` : float
        The angle to rotate the point in degrees

    Returns
    -------
    array
        A 4x4 numpy array representing the rotation matrix.

    """

    t = np.pi * theta / 180
    c, s = np.cos(t), np.sin(t)
    return np.array([[1, 0,  0, 0],
                     [0, c, -s, 0],
                     [0, s,  c, 0],
                     [0, 0,  0, 1]], dtype=float)


def yrotate(theta):
    """
    Creates a rotation matrix that rotates a point around the y-axis by a given angle.

    Parameters
    ----------
    `theta` : float
        The angle to rotate the point in degrees

    Returns
    -------
    array
        A 4x4 numpy array representing the rotation matrix.

    """

    t = np.pi * theta / 180
    c, s = np.cos(t), np.sin(t)
    return  np.array([[ c, 0, s, 0],
                      [ 0, 1, 0, 0],
                      [-s, 0, c, 0],
                      [ 0, 0, 0, 1]], dtype=float)

def get_mvp_matrix(params) -> np.array:
    """
    Computes the Model-View-Projection (MVP) matrix for a given set of transformation
    parameters.

    Parameters
    ----------
    `params` : dict
        A dictionary with the following keys

        `x angle` : float
            The angle to rotatae the object around the x-axis (in degrees)
        `y angle` : float
            The angle to rotatae the object around the y-axis (in degrees)
        `x`, `y`, `z` : float
            The translation of the object along the x, y, and z axes
        `aspect` : float
            The aspect ratio of the viewport (width/height)
        `z near`, `z far` : float
            The distances to the near and far clipping planes

    Returns
    -------
    array
        A 4x4 numpy array representing the MVP matrix.

    """

    model = xrotate(params['x angle']) @ yrotate(params['y angle'])
    view  = translate(params['x'], params['y'], params['z'])
    proj  = perspective(params['y FOV'], params['aspect'], params['z near'], params['z far'])
    mvp   = proj  @ view  @ model

    return mvp

def visualize(vertices, faces, params) -> None:
    """
    Displays a 3D visualization of a mesh of vertices and faces, transformed by a set of
    transformation parameters.

    Parameters
    ----------
    `vertices` : list
        A list of (x, y, z) coordinates for each vertex in the mesh
    `faces`: list
        A list of lists of indicees into the vertices list, specifying the vertices for each
        face in the mesh
    `params`: dict
        A dictionary of transformation parameters to be passed to

    """

    mvp = get_mvp_matrix(params)

    V, F = np.array(vertices), np.array(faces) - 1
    V = (V - (V.max(0) + V.min(0)) / 2) / max(V.max(0) - V.min(0))
    V = np.c_[V, np.ones(len(V))] @ mvp.T
    V /= V[:, 3].reshape(-1,1)
    T = V[F][..., :2]

    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_axes([0, 0, 1, 1], xlim=[-1, +1], ylim=[-1, +1], aspect=1)
    collection = PolyCollection(T, closed=True, linewidth=0.1, facecolor='none', edgecolor='black')
    ax.add_collection(collection)
    plt.show()