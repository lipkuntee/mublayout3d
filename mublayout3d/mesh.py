from .mesh_visualizer import visualize


class Mesh:
    def __init__(self, vertices: list, faces: list) -> None:
        """
        Initializes a mesh with vertices and faces.

        Parameters
        ----------
        vertices : list
            A list of vertices in the form of (x, y, z) or (x, y) coordinates
        faces : list
            A list of faces in the form of (i, j, k) indices, where i, j, and k are the indices
            of the vertices that make up the face

        """
        
        self.vertices = vertices
        self.faces = faces

        # Sets up the default view parameters
        self.view_params = {
            'x'         :    0,
            'y'         :    0,
            'z'         :   -2,
            'x angle'   :  -50,
            'y angle'   :    0,
            'y FOV'     :   25,
            'aspect'    :    1,
            'z near'    :    1,
            'z far'     :  100
        }

    def visualize(self) -> None:
        """
        Visualizes the mesh using the view parameters defined for the Mesh object.

        """

        visualize(self.vertices, self.faces, self.view_params)

    def save_as_obj(self, filename: str) -> None:
        """
        Saves the mesh as an OBJ file with the given filename.

        Parameters
        ----------
        filename : PathLike
            The name of the file to save the mesh to

        """

        is2d = len(self.vertices[0]) == 2

        with open(filename, 'w') as fp:
            fp.write('# OBJ file\n')

            # Writes each vertex to the file with the OBJ syntax
            if is2d:
                for vert in self.vertices:
                    fp.write(f'v {vert[0]} {vert[1]} 0\n')
            else:
                for vert in self.vertices:
                    fp.write(f'v {vert[0]} {vert[1]} {vert[2]}\n')

            # Writes each face to the file with the OBJ syntax
            for face in self.faces:
                fp.write(f'f {face[0]} {face[1]} {face[2]}\n')

            fp.write('\n')