from typing import Any

import tripy

from .mesh import Mesh


def _create_pairs(points: tuple) -> list:
    """
    Create pairs of points from the given list of points.

    Parameters
    ----------
    points : tuple
        A tuple of points

    Returns
    -------
    list
        A list of paired points

    """

    # Create pairs of points from the list
    return [[points[i], points[(i + 1) % len(points)]] for i in range(len(points))]


def _contour_to_vertices(contour: list) -> list:
    """
    Transform the data structure of the given contour points to a list of vertices.

    Parameters
    ----------
    contour : list
        A list of contour points

    Returns
    -------
    list
        A list of vertices

    """
     
    # Transform the data structure of the contour points
    xs = [point[0][0] for point in contour]
    ys = [point[0][1] for point in contour]
    points = [(xs[i], ys[i]) for i in range(len(xs))]

    return points


def _extrude(vertices: list, z: int) -> list:
    """
    Extrude the mesh in the z-direction from the given list of vertices.

    Parameters
    ----------
    vertices : list
        A list of vertices
    z : int
        A value representing the height of the mesh

    Returns
    -------
    list
        A list of extruded faces

    """

    # Extrude the mesh in z-direction from the pair of points
    faces = []
    pairs = _create_pairs(vertices)
    has_depth = len(pairs[0][0]) == 3

    for p in pairs:
        if has_depth:
            faces.append(tuple([(p[1][0], p[1][1], p[1][2]), (p[0][0], p[0][1],  p[0][2]), (p[0][0], p[0][1], p[0][2] + z)]))
            faces.append(tuple([(p[0][0], p[0][1], p[0][2] + z), (p[1][0], p[1][1], p[1][2] + z), (p[1][0], p[1][1], p[1][2])]))
        else:
            faces.append(tuple([(p[1][0], p[1][1], 0), (p[0][0], p[0][1], 0), (p[0][0], p[0][1], z)]))
            faces.append(tuple([(p[0][0], p[0][1], z), (p[1][0], p[1][1], z), (p[1][0], p[1][1], 0)]))

    return faces


def _create_floor(vertices: list, depth: int) -> tuple:
    """
    Create a floor which covers the whole model.

    Parameters
    ----------
    vertices : list
        A list of vertices
    depth : int
        A value representing the depth of the floor

    Returns
    -------
    tuple
        A tuple containing the floor vertices and faces

    """

    # Create a floor which covers the whole model
    max_x = max([v[0] for v in vertices])
    max_y = max([v[1] for v in vertices])
    min_x = min([v[0] for v in vertices])
    min_y = min([v[1] for v in vertices])

    # Create a top floor face
    top_face_vertices = [(min_x, min_y, 0), (min_x, max_y, 0), (max_x, max_y, 0), (max_x, min_y, 0)]
    top_face = [
        (top_face_vertices[2], top_face_vertices[1], top_face_vertices[0]),
        (top_face_vertices[0], top_face_vertices[3], top_face_vertices[2])]

    # Create bottom floor face
    btm_face_vertices = [
        (min_x, min_y, -depth), (min_x, max_y, -depth),
        (max_x, max_y, -depth), (max_x, min_y, -depth)]
    btm_face = [
        (btm_face_vertices[0], btm_face_vertices[1], btm_face_vertices[2]),
        (btm_face_vertices[2], btm_face_vertices[3], btm_face_vertices[0])
    ]

    # Extrude the floor face towards bottom
    side_face = _extrude(btm_face_vertices, depth)

    return top_face_vertices + btm_face_vertices, top_face + side_face + btm_face


def _build_2d_mesh(contours: tuple) -> tuple:
    """
    Build a 2D mesh of the given contours.

    Parameters
    ----------
    contours : list
        A tuple of contours

    Returns
    -------
    tuple
        A tuple containing the vertices and faces of the 2D mesh

    """

    vertices = []
    faces = []

    for contour in contours:
        # Triangulate the vertices with earclipping technique
        _vertices = _contour_to_vertices(contour)
        _faces = tripy.earclip(_vertices)

        # Append to the results
        vertices += _vertices
        faces += _faces

    return vertices, faces


def _build_3d_mesh(contours: tuple, height: int, floor_depth: int) -> tuple[list]:
    """
    Build a 3D mesh of the given contours.

    Parameters
    ----------
    contours : list
        A tuple of contours
    height : int
        A value representing the height of the mesh
    floor_depth : int
        A value representing the depth of the floor

    Returns
    -------
    tuple
        A tuple containing the vertices and faces of the 3D mesh

    """

    # Get the 2D mesh of the contours
    vertices, faces = _build_2d_mesh(contours)

    # Add z-dimension to the vertices and faces
    vertices  = [(v[0], v[1], 0) for v in vertices]
    vertices += [(v[0], v[1], height) for v in vertices]
    faces = [tuple([(p[0], p[1], height) for p in f]) for f in faces]

    # Create the walls for the model
    for contour in contours:
        _vertices = _contour_to_vertices(contour)
        faces += _extrude(_vertices, height)

    # Create the floor for the model
    if floor_depth:
        floor_vertices, floor_faces = _create_floor(vertices, floor_depth)
        vertices += [v for v in floor_vertices if v not in vertices]
        faces += floor_faces

    return vertices, faces


def build_mesh(contours, height: float = None, floor_depth: Any = None) -> Mesh:
    """
    Build a 2D or 3D mesh object of the given contours. A 3D mesh is built if the height
    is specified, otherwise, a simple 2D mesh is returned

    Parameters
    ----------
    contours : list
        A tuple of contours
    height : int
        A value representing the height of the mesh, defaults to None
    floor_depth : int
        A value representing the depth of the floor, defaults to None

    Returns
    -------
    Mesh
        A Mesh object containing the vertices and faces of the 2D or 3D mesh

    """

    # Leave out the contours with only two or less points
    filtered = [contour for contour in contours if len(contour) > 2]

    vertices, faces = _build_3d_mesh(filtered, height, floor_depth) if height != None else _build_2d_mesh(filtered)
    faces = [[vertices.index(p) + 1 for p in f] for f in faces]

    return Mesh(vertices, faces)