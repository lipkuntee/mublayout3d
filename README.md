# MUBLayout3D: Transforming 2D Mixed-use Building Floor Plan Images to 3D Models using Data-driven Methods
By Lip Kun Tee and Nikita Kurkin

## Introduction
Generating 3D models from rasterised 2D floor plan images has always been challenging
for researchers. These architectural drawings consist of auxiliary information objects, such as
walls, doors, windows, furniture, various texts and more. Numerous studies and experiments
have been conducted on residential floor plans. However, the research focussing on mixed-use
buildings could be more extensive. It is already challenging to generate a 3D model from
residential floor plans. The irregularity of the shape of the walls and rooms of mixed-use
building floor plans adds another layer of difficulty on top of it. Therefore, we propose
MuBLayout3D, which uses data-driven deep learning methods to solve the problem.

<div align="center">
    <img src="examples/results.png" width=400 alt="examples">
</div>

To learn more, please refer to our [paper](http://lipkun.codes/docs/mublayout3d/paper) or project [poster](http://lipkun.codes/docs/mublayout3d/poster).

## Updates
[16/2/2023] Uploaded the dataset which are used to train the default models.

## Requirements
The model is written with Python 3.10.4 and TensorFlow 2.9.1 with CUDA enabled GPU. Other dependencies of Python can be found in the `requirements.txt` file. The latest version of TensorFlow seems to cause some known conflicting [issue](https://github.com/qubvel/segmentation_models/issues/374) with the Segmentation Models library. You may have to revert the version of your Python or TensorFlow when using the code.

To install the required dependencies, run the following command on your terminal:
```
pip install -r requirements.txt
``` 

## Usage
To generate a 3D model from a 2D floor plan image, simply run:
```
python generate.py -i </path/to/image> -o </path/to/output>
```
You can also use it in a Python environment by importing the module:
```
import mublayout3d as ml3

# ...
```
For more details of the usage of each function in the code, please look at `Workflow.ipynb` to see how they work.

## Dataset
We have collected a total number of 100 mixed-use building floor plans with different categories, such as educational hubs, hospitals, museums, shopping malls ans transit hubs.

The dataset collected are uploaded and stored in Google Drive which can be downloaded [here](https://drive.google.com/file/d/1_nOYIehpoIg1dAaZYDLi7Tdxk09nRW4P/view). This is the file structure of the dataset:

```
.
├── Object Detection Annotations
│   ├── labels.txt
│   └── ...
├── Others
│   ├── 000.jpg
│   └── ...
├── Raw Floor Plan Images
│   ├── 000.jpg
│   └── ...
├── Wall Mask
│   ├── 000.jpg
│   └── ...
└── Dataset information.xlsx

4 directories, 168 files
```
The dataset contains 60 wall masks and 100 annotations for the object detection task with 3 classes (windows, single- and double-hinged doors).

## Acknowledgement
Thank you Dr Rohit K. Dubey and Andrea Carrara for providing helpful advice and feedback throughout the project.

## Contact
If you have any questions, please contact us at lipkun.tee@tum.de or nikita.kurkin@tum.de.

## License
MUBLayout3D has a MIT-style license, as found in the [LICENSE](https://gitlab.com/lipkuntee/mublayout3d/-/blob/main/LICENSE) file.
