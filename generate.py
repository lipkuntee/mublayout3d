import os
import argparse


def main():
    parser = argparse.ArgumentParser(description='Generate a 3D model from a 2D rasterized floor plan image.')
    parser.add_argument('-i', '--image', type=str, help='Path to the floor plan image file', required=True)
    parser.add_argument('-o', '--output', type=str, default='', help='Path to save the 3D model')
    parser.add_argument('-w', '--wall', type=int, default=100, help='Desired height of the walls')
    parser.add_argument('-f', '--floor', type=int, default=10, help='Desired depth of the floor')
    args = parser.parse_args()

    if not os.path.isfile(args.image):
        print(f'The specified path to the floor plan image is invalid, PATH: {args.image}')
        exit(1)

    from mublayout3d import generate_model

    generate_model(args.image, args.output, args.wall, args.floor)


if __name__ == '__main__':
    main()